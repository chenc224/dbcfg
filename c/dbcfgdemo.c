#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include "dbcfg.h"

void pmem(){
    char s[100];
    sprintf(s,"pmap -d %d|tail -n 1",getpid());
    system(s);
    puts("");
}

void smp1(char * cfgname,char *name) {//样例1，读入数据
    struct s_dbcfg dbc; //定义一个结构体
    int i;
    dbcfg_init(&dbc);   //初始化
    dbc.read2(cfgname,name);   //读入命令行参数中指定的配置文件
    
    for(i=0;i<dbc.tnum;i++){
        printf("t数组%d:%s\n",i,dbc.t[i]);
    };
    for(i=0;i<dbc.dnum;i++){
        printf("d数组%s:%d|%s\n",dbc.dt[i],dbc.di[i],dbc.ds[i]);
    };

    printf("根据名称直接查找显示\n");
    printf("user对应字符串:%s\n",dbc.gets("User","没找到"));
    printf("port对应int:%d\n",dbc.geti("port",-1));
}

void smp2(char * cfgname,char *name) {//样例1，读入数据
    struct s_dbcfg dbc; //定义一个结构体
    dbcfg_init(&dbc);   //初始化
    dbc.read2(cfgname,name);   //读入命令行参数中指定的配置文件
}

int main(int argc, char **argv) {
    int i,cscs=1000000;
    time_t kssj;
    if(argc<2)  {
        puts("需要一个参数指定配置文件名");
        return -1;
    };
    if(argc==2) {
        smp1(argv[1],NULL);
    };
    if(argc==3) {
        smp1(argv[1],argv[2]);
        puts("检查是否有内存泄露");
        kssj=time(NULL);
        pmem();
        for(i=0;i<cscs;i++)    {
            smp2(argv[1],argv[2]);
        };
        pmem();
        printf("执行%d次，花时间%ld秒\n",cscs,time(NULL)-kssj);
    };
    return 0;
}
