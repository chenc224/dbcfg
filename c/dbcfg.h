#ifndef dbcfg_h
#define dbcfg_h


#ifdef __cplusplus
extern "C"  {
#endif

#define dbcfg_ITEM_NUM  10
#define dbcfg_ITEM_SIZE 100

struct s_dbcfg  {
    int (*read)(char * cfgname);//读入配置，相当于read2的第二个参数为NULL
    int (*read2)(char * cfgname,char * name2);//读入配置到内存
    
    int (*geti)(char * itemname,int deft);//获取d数组标题为itemname的项对应的int值
    char * (*gets)(char * itemname,char * deft);//获取d数组标题为itemname的项对应的串指针

    char t[dbcfg_ITEM_NUM][dbcfg_ITEM_SIZE];//配置文件中t的内容
    char dt[dbcfg_ITEM_NUM][dbcfg_ITEM_SIZE],ds[dbcfg_ITEM_NUM][dbcfg_ITEM_SIZE];//配置文件中d的标题，内容
    int di[dbcfg_ITEM_SIZE];//配置文件中d的int内容，一般来说是端口这种
    char db[dbcfg_ITEM_SIZE];//记录配置中db内容
    int tnum,dnum;//t数组和d数组数量

    char cfgname[100];//保存配置的名字
    int rtcode;//返回码
    char rtinfo[1000];//返回信息
};

int dbcfg_init(struct s_dbcfg *);

#ifdef __cplusplus
}
#endif

#endif
